# Mutating functions

original = [1, 5, 3, 4, 2]
sorted = sort(original)
println(original, sorted)
# [1, 5, 3, 4, 2][1, 2, 3, 4, 5]
sort!(original)
println(original)
# [1, 2, 3, 4, 5]


# Broadcast

A = [1, 2, 3]

C = 2 .* A .+ sin.(A)



D = @. 2A + sin(A)





# macros
using Distributed

@time n_tails = @distributed (+) for i=1:1000000
   rand(Bool)
end
#   0.048812 seconds (63.55 k allocations: 3.048 MiB)


# Language plot
using Plots
pyplot()
using PyPlot
xkcd()
x=[1, 0.9, 5.0, 4.9]; y=[4.8, 5.2, 0.5, 0.9]
labels=["C", "Fortran", "Python", "R"]
Plots.scatter(x, y,
        series_annotations=Plots.text.(labels, :bottom),
        xlims=(0, 6),
        ylims=(0, 6),
        leg=false,
        xlabel="CONVENIENCE",
        ylabel="SPEED",
        tickfontcolor=:white)

x = [1, 0.9, 5.0, 4.9, 5.1]; y = [4.8, 5.2, 0.5, 0.9, 4.7]
labels=["C", "Fortran", "Python", "R", "Julia"]
Plots.scatter(x, y,
        series_annotations=Plots.text.(labels, :bottom),
        xlims=(0, 6),
        ylims=(0, 6),
        leg=false,
        xlabel="CONVENIENCE",
        ylabel="SPEED",
        tickfontcolor=:white)

const y = 2

function sum_power_of_self(x::Int, y::Int)
        return x^x + y^y
end

sum_power_of_self(x, y)
sum_power_of_self(1, 2.5)
