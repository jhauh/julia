# Multiple dispatch
# * operator dispatches to different functions depending on arguments
"Hello " * "World"
100_000 * 7

methods(*)


# User can take advantage of multiple dispatch:
function hello(name::String)
    println("Hi my name is " * name)
end

function hello(name::String, age::Int)
    println("Hi my name is " * name * " and I am " * string(age))
end

function hello()
    println("Hello")
end

hello()
hello("World")
hello("World", 6000)





# Specialization
function score(a::Int, b::String)
    c = convert(String, a)
    print(c * b)
end

function mark(a, b)
    c = convert(String, a)
    print(c * b)
end


score_native = @code_native score(10, " out of 10!")
mark_native = @code_native mark(10, " out of 10!")

score_native == mark_native
