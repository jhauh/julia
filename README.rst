Julia Sales Pitch
=================

Slides and code used for a seminar on the Julia programming language I
gave a couple of times in early 2020, at which point Julia was on V1 - 1.1.

Duration:
  60 minutes

Intended Audience:
  Data scientists with R/Python knowledge


To Do
-----

- Rewrite the slides in Julia, as an exercise.
- Update equivalent package recommendations


