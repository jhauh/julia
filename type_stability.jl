function count_numbers1(v)
    n = length(v)
    d = Dict()
    for i = 1:n
        d[v[i]] = get(d, v[i], 0) + 1
    end
    return d
end

function count_numbers2(v)
    n = length(v)
    d = Dict{Int, Int}()
    for i = 1:n
        d[v[i]] = get(d, v[i], 0) + 1
    end
    return d
end

v1 = rand([1, 2, 3], 100_000);
v2 = rand([1, 2, 3], 100_000_000);

count_numbers1(v1)
count_numbers2(v1)

@time count_numbers1(v1)
@time count_numbers2(v1)

@time count_numbers1(v2)
@time count_numbers2(v2)
